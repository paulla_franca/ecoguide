# README #

## Resumo ##

O EcoGuide é um sistema voltado à elaboração de roteiros para quem deseja fazer viagens ecológicas. Objetiva criar uma aplicação gratuita que facilite o trabalho de pessoas que desejam viajar, mas não possuem tempo para elaborar o planejamento ou conhecimento prévio das atrações turísticas, possíveis de serem visitadas no local desejado.
Versão Final

## Configuração ##

Após baixar o arquivo do sistema é necessário colocá-lo em um servidor HTTP, ex.: Apache. 
É utilizado o Framework AngularJS e máscaras de input em JQuery.
Algumas APIs foram utilizadas: Google Maps JavaScript API(Google Places) e Kendo jQuery Scheduler.
Para utilização do Google Places é necessária uma chave de acesso da Google.
Na utilização da API do Kendo Scheduler algumas alterações foram feitas para deixar a API em português.
Não é utilizado Banco de Dados.

### Implementação ###
A inicialização do uso do sistema começa com a página index, a qual possui links e scripts que são utilizados em todo o sistemas.
Através das rotas do AngularJS *(assets/js/angular.js)* as views *(view/...)* são redirecionadas.
O template *(view/template/index.php)* possui a diretiva **ui-view** que mostra a view de acordo com a rota da url. 
Cada view possui um controller *(assets/js/controllers/...)* para poder modificar a página de forma dinâmica.
Durante toda a utilização do sistema utilizamos a variável **$rootScope.infoUser** para armazenas os dados selecionados pelo usuário.

### Contribuidores ###

Blenda Paula da Silva Medeiros - blenda.paula@escolar.ifrn.edu.br
Kalyane de Oliveira Bezerra - kalyane.oliveira@escolar.ifrn.edu.br
Lucas Medeiros Gomes - medeiros.gomes@escolar.ifrn.edu.br
Mariana Lima Taniguchi - mariana.taniguchi@escolar.ifrn.edu.br
Paulla Beatriz França de Sousa - paulla.sousa@escolar.ifrn.edu.br