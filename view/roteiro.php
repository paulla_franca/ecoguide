<div id="estadia">
  <div class="menu">
    <div class="container">
      <ul>
        <li style="color: #444;">
          <i class="fa fa-globe" aria-hidden="true"></i> &nbsp&nbsp ESTADIA
        </li>
        <li style="color: #444;">
          <i class="fa fa-bed" aria-hidden="true"></i> &nbsp&nbsp HOTÉIS
        </li>
        <li style="color: #444;">
          <i class="fa fa-cutlery" aria-hidden="true"></i> &nbsp&nbsp RESTAURANTES
        </li>
        <li style="color: #444;">
          <i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp&nbsp ATRAÇÕES
        </li>
        <li class="active">
          <i class="fa fa-calendar" aria-hidden="true"></i> &nbsp&nbsp ROTEIRO
        </li>
      </ul>
    </div>
  </div>
  <style>
        /*
            Use the DejaVu Sans font for display and embedding in the PDF file.
            The standard PDF fonts have no support for Unicode characters.
        */
        .k-scheduler {
            font-family: "DejaVu Sans", "Arial", sans-serif;
            font-size: .9em;
        }

        /* Hide toolbar, navigation and footer during export */
        .k-pdf-export .k-scheduler-toolbar >{
            display: none;
        }
        .k-pdf-export .k-scheduler-navigation .k-nav-today,
        .k-pdf-export .k-scheduler-navigation .k-nav-prev,
        .k-pdf-export .k-scheduler-navigation .k-nav-next,
        .k-pdf-export .k-scheduler-footer
        {
            display: none;
        }
    </style>

    <script>
        // Import DejaVu Sans font for embedding

        // NOTE: Only required if the Kendo UI stylesheets are loaded
        // from a different origin, e.g. cdn.kendostatic.com
        kendo.pdf.defineFont({
            "DejaVu Sans"             : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans.ttf",
            "DejaVu Sans|Bold"        : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Bold.ttf",
            "DejaVu Sans|Bold|Italic" : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
            "DejaVu Sans|Italic"      : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
            "WebComponentsIcons"      : "https://kendo.cdn.telerik.com/2017.1.223/styles/fonts/glyphs/WebComponentsIcons.ttf"
        });
    </script>

    <!-- Load Pako ZLIB library to enable PDF compression -->
    <script src="https://kendo.cdn.telerik.com/2017.3.1018/js/pako_deflate.min.js"></script>

  <div id="example">
        <div kendo-scheduler k-options="schedulerOptions" ng-controller="calendarController">
            <div id="scheduler">
            </div>
        </div>
        <style>
            .custom-event {
                color: #fff;
                text-shadow: 0 1px 0 #000;
            }
            .custom-all-day-event {
                text-align: center;
                text-transform: uppercase
            }
        </style>
    </div>

    <script id="event-template" type="text/x-kendo-template">
        <div class="movie-template" style="width: 100%; height: 100%;" ng-style="#= OwnerID # == 1 && {'background-color': 'rgb(81, 160, 237)'}  || #= OwnerID # == 2 && {'background-color': 'rgb(248, 163, 152)'}  || #= OwnerID # === 3 && {'background-color':'rgb(86, 202, 133)'}">
            <div style="background-image:url(#= image #);background-size: 100% 100%; position: relative; width: 100%; height: 100%;">
                <div style="position: absolute; width: 100%; height: 100%;" ng-style="#= OwnerID # == 1 && {'background-color': 'rgba(81, 160, 237, 0.7)'}  || #= OwnerID # == 2 && {'background-color': 'rgba(248, 163, 152, 0.7)'}  || #= OwnerID # === 3 && {'background-color':'rgba(86, 202, 133, 0.7)'}">
                    <p>#: kendo.toString(start, "hh:mm") # - #: kendo.toString(end, "hh:mm") #</p>
                    <h4>#: title #</h4>
                    <p>#: formatted_address #</p>  
                </div>
            </div>
        </div>
    </script>
</div>

