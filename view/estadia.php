  <div id="estadia">
    <div class="menu">
      <div class="container">
        <ul>
          <li class="active">
            <i class="fa fa-globe" aria-hidden="true"></i> &nbsp&nbsp ESTADIA
          </li>
          <li>
            <i class="fa fa-bed" aria-hidden="true"></i> &nbsp&nbsp HOTÉIS
          </li>
          <li>
            <i class="fa fa-cutlery" aria-hidden="true"></i> &nbsp&nbsp RESTAURANTES
          </li>
          <li>
            <i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp&nbsp ATRAÇÕES
          </li>
          <li>
            <i class="fa fa-calendar" aria-hidden="true"></i> &nbsp&nbsp ROTEIRO
          </li>
        </ul>
      </div>
    </div>
    <div style="text-align: center; margin-bottom: 60px;">
      <h1 style="font-family: 'Merriweather', serif; color: white; font-size: 48px; line-height: 48px; font-weight: 400;">FAÇA SEU ROTEIRO</h1>
      <p style="color: white; margin: 20px; font-family: 'Open Sans', sans-serif; font-size: 20px;">Defina sua <b>estadia</b></p>
    </div>
    <div class="container">
        <div class="row">
          <div class="col-sm-3 col-xs-12" style="padding: 0 20px">
            <label style="font-size: 17px;"><b>LOCAL</b></label>
            <input type="text" class="form-control" id="destino" ng-model="destino" placeholder="DESTINO">
          </div>
          <div class="col-sm-3 col-xs-12" style="padding: 0 20px">
            <label style="font-size: 17px;"><b>INÍCIO</b></label>
            <input type="text" ng-model="initialDay" id="initialDay" class="form-control" placeholder="DD/MM/AAAA" mask='39/19/9999'>
          </div>
          <div class="col-sm-3 col-xs-12" style="padding: 0 20px">
            <label style="font-size: 17px;"><b>FIM</b></label>
            <input type="text" ng-model="finalDay" id="finalDay" class="form-control" placeholder="DD/MM/AAAA" mask='39/19/9999'>
          </div>
          <div class="col-sm-3 col-xs-12" style="padding: 0 20px">
            <label style="font-size: 17px;"><b>RAIO DE PESQUISA</b></label>
            <input type="range" ng-model="distancia" min="100" max="50000">
            <p class="price">{{distancia}} Metros</p>
          </div>                      
        </div>
        <div style="text-align: center;" class="botao2"><a style="margin-top: 40px;" ng-click="inserir()">Próxima Etapa</a></div>
        <style type="text/css">
          .botao2 a{
          font-family: 'Merriweather', serif;
            text-decoration: none;
            background-color: rgba(88,179,43,1.00);
            padding: 15px 40px;
            color: white !important;
            font-size: 25px;
            margin-bottom: 80px;
            display: inline-block;
          }
           .botao2 a:hover{
            color: white !important;
            background-color: rgba(71,155,30,1.00);
            cursor: pointer;
          }
        </style>
    </div>
  </div>