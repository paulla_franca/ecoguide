  <div id="back">
    <div id="header">
      <div id="logo" class="pull-left"><a class="linkHome" ui-sref="home"><img src="assets/images/ecoguidelogo.png" class="img-responsive" style="height: 40px;"></a></div>
    </div>
    <div style="text-align: center;" class="texto_inicial animated zoomIn">
      Monte seu roteiro de<br>
      viagem ecológica<br>
    </div>
    <div class="animated fadeInUp">
      <div class="texto_baixo">Construa cronogramas personalizados de acordo com suas preferências</div>
      <div style="text-align: center;" class="botao"><a href="#!/#sistema">Iniciar Roteiro</a></div>
    </div>
<style type="text/css">
body{
  overflow-x: hidden;
}
   .botao a{
    font-family: 'Merriweather', serif;
        text-decoration: none;
        background-color: rgba(88,179,43,1.00);
        padding: 15px 40px;
        color: white;
        font-size: 25px;
        margin-bottom: 180px;
        display: inline-block;
      }
       .botao a:hover{
        color: white;
        background-color: rgba(71,155,30,1.00);
      }
      .texto_inicial{
        font-size: 70px;
        color: white;
        padding: 180px 20px 40px 20px;
        font-family: 'Merriweather', serif;
      }
      .texto_baixo{
        text-align: center;
        color: white;
        font-size: 20px;
        font-family: 'Open Sans', sans-serif;
        margin-bottom: 40px;
      }
    </style>

  </div>

  <!-- Fim: etapas para fazer o roteiro -->

  <div id="etapas" style="padding-top: 80px; padding-bottom: 80px;">
    <div class="container" style="text-align: center;">
      <h2 style="font-family: 'Merriweather', serif; color: #409843; font-size: 48px; line-height: 48px; font-weight: 400;">Escolha as melhores opções para a sua viagem</h2>
      <p style="color: #486350; margin: 20px; font-family: 'Open Sans', sans-serif; font-size: 20px;">Visualize locais e as atividades disponíveis, de acordo com suas preferências, com preços, horários, disponibilidade, dentre outros. Ao final de suas escolhas, terá acesso ao roteiro personalizado e gratuito!</p>
    <div class="row" style="margin-top: 40px;">
      <div class="col-md-4 etapa">
        <img class="img-responsive" src="assets/images/hotel.png" alt="">
        <div class="">
          <h4>Hotéis</h4>
          <hr>
        </div>
      </div>
      <div class="col-md-4 etapa">
        <img class="img-responsive" src="assets/images/restaurante.png" alt="">
        <div class="">
          <h4>Restaurantes</h4>
          <hr>
        </div>
      </div>
      <div class="col-md-4 etapa">
        <img class="img-responsive" src="assets/images/atracao.png" alt="">
        <div class="">
          <h4>Atrações</h4>
          <hr>
        </div>
      </div>
    </div>
    </div>
    <style media="screen">
      .etapa{
        text-align: left;
      }
      .etapa h4{
        color: #3b4b50;
        font-family: 'Merriweather', serif;
        font-size: 25px;
      }
      .etapa img{
        opacity: .9;
        transition: all .3s ease-in-out;
      }
      .etapa:hover img{
        opacity: 1;
      }
      .etapa hr{
        border: 0.1em solid transparent;
        margin-left: 0;
        background-color: white;
        width: 0%;
        transition: all .3s ease-in-out;
      }
      .etapa:hover hr{
        width: 40%;
        border: 0.1em solid #58b32b;
      }
    </style>
  </div>
  <style media="screen">
    #sistema{
      height: 100%;
      width: 100%;
      padding: 0;
      margin: 0;
      background: black url("assets/images/background.jpeg") center center no-repeat;;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-attachment: fixed;
    }
  </style>
  <div id="sistema">
    <ui-view></ui-view>
  </div>
  <div class="section mcb-section" style="padding-top:30px; padding-bottom:30px; background-color:#f0eee4; font-family: 'Merriweather', serif; color: #3b4b50; text-align: center;">
    <h4 style="font-size: 22px;">É uma satisfação tê-lo aqui conosco!</h4>
  </div>

  <div class="section mcb-section equal-height-wrap full-width " style="padding-top:0px; padding-bottom:0px; background-color:#ccc">
    <div class="row">
      <div class="wrap mcb-wrap three-fifth  valign-top clearfix col-md-7" style="background-color: rgb(9, 55, 37); background-image: url(&quot;assets/images/mato.jpeg&quot;); background-repeat: no-repeat; background-position: center top; height: 558px;">
      </div>
      <div class="wrap mcb-wrap two-fifth  column-margin-30px valign-top clearfix col-md-5" style="padding: 80px 50px 65px; background-color: rgb(83, 154, 54); background-image: url(&quot;http://betheme.muffingroupsc.netdna-cdn.com/be/eco/wp-content/uploads/2016/04/home_eco_section_box.png&quot;); background-repeat: no-repeat; background-position: center top; height: 558px;">
        <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_column  column-margin-">
            <div class="column_attr clearfix" style="">
              <h2 style="color: #fff; word-wrap: break-word; font-family: 'Merriweather', serif;">Porque utilizar o<br>ECOGUIDE?</h2>
              <hr class="no_line" style="margin: 0 auto 10px;">
              <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                <div class="image_wrapper">
                  <img class="scale-with-grid" src="http://betheme.muffingroupsc.netdna-cdn.com/be/eco/wp-content/uploads/2016/04/home_eco_sep1.png" alt="home_eco_sep1" width="119" height="2">
                </div>
              </div>
              <hr class="no_line" style="margin: 0 auto 25px;">
              <h5 style="color: #fff; font-size: 17px; font-family: 'Open Sans', sans-serif;">Veja abaixo os três pincipais motivos: </h5>
            </div>
          </div>
          <div style="font-size: 17px; font-family: 'Open Sans', sans-serif;">
            <div class="column mcb-column one column_column  column-margin-">
            <div class="column_attr clearfix" style=" background-image:url('http://betheme.muffingroupsc.netdna-cdn.com/be/eco/wp-content/uploads/2016/04/home_eco_list_num1.png'); background-repeat:no-repeat; background-position:left top; padding:5px 0 0 70px;">
              <p class="big" style="color: #fff; margin: 0; font-weight: 300;">Auxilia quem tem dificuldade na construção de um cronograma de viagens.</p>
            </div>
          </div>
          <div class="column mcb-column one column_column  column-margin-">
            <div class="column_attr clearfix" style=" background-image:url('http://betheme.muffingroupsc.netdna-cdn.com/be/eco/wp-content/uploads/2016/04/home_eco_list_num2.png'); background-repeat:no-repeat; background-position:left top; padding:5px 0 0 70px;">
              <p class="big" style="color: #fff; margin: 0; font-weight: 300;">Disponibiliza uma série de lugares e suas informações provindas do Google Maps.</p>
            </div>
          </div>
          <div class="column mcb-column one column_column  column-margin-">
            <div class="column_attr clearfix" style=" background-image:url('http://betheme.muffingroupsc.netdna-cdn.com/be/eco/wp-content/uploads/2016/04/home_eco_list_num3.png'); background-repeat:no-repeat; background-position:left top; padding:5px 0 0 70px;">
              <p class="big" style="color: #fff; margin: 0; font-weight: 300;">Feito especialmente para o Ecoturista que existe dentro de você.</p>
            </div>
          </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <footer>
    <div style="margin-top:45px;">
      <div class="row" style="font-family: 'Open Sans', sans-serif; text-align:left;margin: 0 10%;margin-bottom:60px;">
        <div class="col-md-3">
          <a href="http://fases.ifrn.edu.br/ecoguide/"><img src="assets/images/ecoguidelogo.png" class="img-responsive" style="height: 40px;"></a>
          <p class="espaco">O EcoGuide é um sistema voltado à elaboração de roteiros para quem deseja fazer viagens ecológicas.</p>
          <div style="font-size: 28px; line-height: 28px;" class="row"> 
            <div class="col-md-2"></div>
            <div class="col-md-2"><a target=“_blank” href="https://www.facebook.com/informaticazn/" style="color: #58b32b;" href="#"><i class="fa fa-facebook"></i></a></div> 
            <div class="col-md-2"><a target=“_blank” style="color: #58b32b;" href="#"><i class="fa fa-google-plus"></i></a></div>
            <div class="col-md-2"><a target=“_blank” style="color: #58b32b;" href="#"><i class="fa fa-twitter"></i></a></div>
            <div class="col-md-2"><a target=“_blank” style="color: #58b32b;" href="#"><i class="fa fa-instagram"></i></a></div>
            <div class="col-md-2"></div>
          </div>
          <p class="espaco">Telefone: +55 (84) 4006-9500<br>Email: <a href="#" style="color: #58b32b">gabin.zn@ifrn.edu.br</a></p>
        </div>
        <div class="col-md-3">
          <h4 style="font-size: 22px; font-family: 'Merriweather', serif; margin: 0"><img src="assets/images/logoIF.png" style="height: 40px;"> IFRN</h4>
          <p class="espaco">Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Norte Natal - Zona Norte
</p>
<div class="espaco">Endereço: Rua Brusque, 2926, Conjunto Santa Catarina, Potengi
<br> Website: <a href="http://portal.ifrn.edu.br" style="color: #58b32b">http://portal.ifrn.edu.br</a></div>
        </div>
        <div class="col-md-3">
          <h4 style="font-size: 22px; font-family: 'Merriweather', serif; margin: 0"><img src="assets/images/logoFases.png" style="height: 40px;"> FaSEs</h4>
        <p class="espaco">
FÁBRICA DE SOFTWARE ESCOLA</p>
<p>Os alunos, coordenados por seus professores, tem como objetivo cumprir as demandas levantadas da melhor forma possível.</p>
<p>Website: <a href="http://fases.ifrn.edu.br" style="color: #58b32b">http://fases.ifrn.edu.br</a> </p>
        </div>
        <div class="col-md-3">
          <h4 style="font-size: 22px; font-family: 'Merriweather', serif; margin: 0"><img src="assets/images/edmilson.jpg" style="height: 40px;"> PDSI</h4>
          <p class="espaco">Conhecer e aplicar um processo de desenvolvimento de software para desenvolvimento de sistemas.</p>
          <p class="espaco">Orientador: Edmilson Campos <br>Website: <a href="https://edmilsoncampos.net/" style="color: #58b32b">https://edmilsoncampos.net/</a></p>
        </div>
      </div>
    </div>
    <div  style="border-top: 1px solid rgba(255,255,255,.1); height: 25px; padding-top: 10px; margin-bottom: 10px;">
<p>ECOGUIDE © 2017 | IFRN - Projeto de Desenvolvimento de Sistemas para Internet</p>
    </div>
    <style type="text/css">
    .espaco{
      margin-top: 20px;
    }
    </style>
  </footer>
