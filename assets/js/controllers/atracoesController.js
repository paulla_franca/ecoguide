app.controller("atracoesController", function($scope, $http, $location, $sce, $rootScope, $uibModal, $log, $document, $timeout){
    $scope.categoria = "amusement_park";
    $scope.changePage = function(){
        var ok = true;
        for (var i = $scope.datas.length - 1; i >= 0; i--) {
            var tem = false;
            for (var j = $rootScope.infoUser.atracoes.length - 1; j >= 0; j--) {
                if($scope.datas[i] == $rootScope.infoUser.atracoes[j].dia_escolhido){
                    tem = true;
                }
            }
            if(!tem){
                var r = confirm("Você não selecionou atrações em todos os dias de sua viagem. Deseja continuar?");
                if (r == true) {
                    $location.path('/roteiro');
                    break;
                }else{
                    ok = false;
                    break;
                }
            }
        }
        if(ok)$location.path('/roteiro');
    }

    $scope.dias = function (){
        var initialParts = $rootScope.infoUser.data_inicio.split("/");
        var finalParts = $rootScope.infoUser.data_fim.split("/");

        var dataInicial = new Date(parseInt(initialParts[2]), parseInt(initialParts[1]) - 1, parseInt(initialParts[0]));
        var dataFinal = new Date(parseInt(finalParts[2]), parseInt(finalParts[1]) - 1, parseInt(finalParts[0]));

        var diferencaTempo = Math.abs(dataFinal.getTime() - dataInicial.getTime());
        var qtdDias = Math.ceil(diferencaTempo / (1000 * 3600 * 24));

        $scope.datas = [];

        $scope.datas.push(dataInicial.getDate()+"/"+parseInt(dataInicial.getMonth()+1)+"/"+dataInicial.getFullYear());

        for(var i=0; i<qtdDias; i++){
            dataInicial.setDate(dataInicial.getDate()+1);
            $scope.datas.push(dataInicial.getDate()+"/"+parseInt(dataInicial.getMonth()+1)+"/"+dataInicial.getFullYear());
        }
    }
    $scope.dias();

    $scope.apear = function(){
        $scope.atracoesDetalhe = [];
        
        var service = new google.maps.places.PlacesService(document.createElement('div'));

        service.getDetails({
          placeId: $rootScope.infoUser.destino_id
        }, function(place, status) {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            var local = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
            service.nearbySearch({
                location: local,
                radius: $rootScope.infoUser.distancia,
                type: [$scope.categoria]
            },  function(results, status){
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        service.getDetails({
                            placeId:results[i].place_id
                        }, function(place, status){
                            if (status === google.maps.places.PlacesServiceStatus.OK) {
                                if(typeof place.photos != 'undefined'){
                                    for (var i = place.photos.length - 1; i >= 0; i--) {
                                        place.photos[i] = place.photos[i].getUrl({ 'maxWidth': 205, 'maxHeight': 150 });
                                    }
                                }else{
                                    place.photos = [];
                                    place.photos[0] = "assets/images/image.png";
                                }
                                $scope.atracoesDetalhe.push(place);
                            }
                        });                        
                    }
                }
            });
          }
        });

        $timeout(function () {
            console.log($scope.atracoesDetalhe);
        }, 2000);
    }
    $scope.apear();
	
    $scope.selecionarAtracao = function(a){
        var atracao = {
            "nome": a.name,
            "logradouro": a.formatted_address,
            "telefone": a.formatted_phone_number,
            "website": a.website,
            "dia_escolhido": "",
            "horario_escolhido": "",
            "photos": a.photos,
            "rating": a.rating,
            "opening_hours": a.opening_hours,
            "place_id": a.place_id,
            "formatted_address": a.formatted_address
        };
        $scope.openModal(atracao);
    }

    //Remover a atração da lista
    $scope.removerAtracao = function(a){
        for(var i=0; i<$rootScope.infoUser.atracoes.length; i++){
            if($rootScope.infoUser.atracoes[i].nome == a.nome && $rootScope.infoUser.atracoes[i].dia_escolhido == a.dia_escolhido && 
            $rootScope.infoUser.atracoes[i].horario_escolhido == a.horario_escolhido ){
                $rootScope.infoUser.atracoes.splice(i,1);
            }
        };
    }
    //Editar Dados do Restaurante
    $scope.editarAtracao = function(a){
        for(var i=0; i<$rootScope.infoUser.atracoes.length; i++){
            if($rootScope.infoUser.atracoes[i].nome == a.nome && $rootScope.infoUser.atracoes[i].dia_escolhido == a.dia_escolhido && 
            $rootScope.infoUser.atracoes[i].horario_escolhido == a.horario_escolhido ){
                $scope.openModal(a);
            }
        };
    }

    //Função para abrir modal
    $scope.openModal = function(atracao, size, parentSelector){
        var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'myModalContent2.html',
            controller: 'ModalInstanceCtrl2',
            controllerAs: '$ctrl',
            size: size,
            appendTo: parentElem,
            resolve: {
                atracao: function () {
;                  return atracao;
                }
            }
        });
    }
});

//ModalController -> controller usado dentro do modal (tudo o que tiver dentro da div do modal, as funções para isso (o que está dentro da divModal) ficam dentro desse controller)
app.controller('ModalInstanceCtrl2', function ($rootScope, $scope, $uibModalInstance, $log, atracao) {
  var $ctrl= this;
  $ctrl.atracao = null;
  $ctrl.atracao = atracao;

  $ctrl.dias = function (){
        var initialParts = $rootScope.infoUser.data_inicio.split("/");
        var finalParts = $rootScope.infoUser.data_fim.split("/");

        var dataInicial = new Date(parseInt(initialParts[2]), parseInt(initialParts[1]) - 1, parseInt(initialParts[0]));
        var dataFinal = new Date(parseInt(finalParts[2]), parseInt(finalParts[1]) - 1, parseInt(finalParts[0]));

        var diferencaTempo = Math.abs(dataFinal.getTime() - dataInicial.getTime());
        var qtdDias = Math.ceil(diferencaTempo / (1000 * 3600 * 24));

        $ctrl.datas = [];

        $ctrl.datas.push(dataInicial.getDate()+"/"+parseInt(dataInicial.getMonth()+1)+"/"+dataInicial.getFullYear());

        for(var i=0; i<qtdDias; i++){
            dataInicial.setDate(dataInicial.getDate()+1);
            $ctrl.datas.push(dataInicial.getDate()+"/"+parseInt(dataInicial.getMonth()+1)+"/"+dataInicial.getFullYear());
        }

        console.log($ctrl.datas);
    }
    $ctrl.dias();

    $ctrl.selectHorario = function (horario){
        if(typeof $ctrl.atracao.opening_hours == 'undefined' || typeof $ctrl.atracao.opening_hours.periods == 'undefined'){
            return true;
        }
        var dias = $ctrl.atracao.opening_hours.periods;
        if(dias.length == 1 && dias[0].open.day == 0 && dias[0].open.time == "0000"){
            return true;
        }

        var diaParts = $ctrl.dia.split("/");
        diaParts[0] = parseInt(diaParts[0]);
        diaParts[1] = parseInt(diaParts[1]);
        diaParts[2] = parseInt(diaParts[2]);

        var dataEscolhida = new Date(diaParts[2], diaParts[1] - 1, diaParts[0]);

        for (var i = dias.length - 1; i >= 0; i--) {
            if(dias[i].open.day == dataEscolhida.getDay()){
                var hora = horario.split(":");
                dataEscolhida.setHours(hora[0]);
                dataEscolhida.setMinutes(hora[1]);

                var dataOpen = new Date(dataEscolhida);
                dataOpen.setHours(parseInt(dias[i].open.time.slice(0,2)));
                dataOpen.setMinutes(parseInt(dias[i].open.time.slice(2,4)));

                if(parseInt(dias[i].open.time)>parseInt(dias[i].close.time)){
                    var dataClose = new Date(dataEscolhida);
                    dataClose.setHours(parseInt(dias[i].close.time.slice(0,2)));
                    dataClose.setMinutes(parseInt(dias[i].close.time.slice(2,4)));
                    dataClose.setDate(dataEscolhida.getDate()+1);
                }else{
                    var dataClose = new Date(dataEscolhida);
                    dataClose.setHours(parseInt(dias[i].close.time.slice(0,2)));
                    dataClose.setMinutes(parseInt(dias[i].close.time.slice(2,4)));
                }
                if(dataEscolhida>=dataOpen && dataEscolhida<=dataClose){
                    return true;
                }
            }
        }

        return false;
    }


//Função: Adicionar os dados dos inputs na variável atração e add atração na variável global
  $ctrl.addList = function(){
    var error = 0;
    if($ctrl.dia != null){
        if($ctrl.conferirData() == "invalido"){
            error = 1;
            alert("Data inválida. Tente novamente.");
        }
        else if($ctrl.conferirData() == "valido"){
            $ctrl.atracao.dia_escolhido = $ctrl.dia;
        }
    }
    if($ctrl.mytime != null){
        if($ctrl.dia == "" || $ctrl.dia == null){
            error = 1;
            alert("Não é possível escolher um horário sem data.");
        }
        if($ctrl.conferirHorario() == "invalido"){
            error = 1;
            alert("Horário inválido. Tente novamente.");
        }else if($ctrl.conferirHorario() == "valido"){
            $ctrl.atracao.horario_escolhido = $ctrl.mytime;
        }
    }    
    if(error == 0 && $ctrl.mytime != null && $ctrl.dia != null && $ctrl.mytime != "" && $ctrl.dia != ""){
        $rootScope.infoUser.atracoes.push($ctrl.atracao);
        $uibModalInstance.close("");
    }else{
        alert("Preencha os campos corretamente");
    }
  };

//Função: Fechar o Modal e não salvar o atração selecionado
  $ctrl.cancel = function(){
    $ctrl.atracao = null;
    $uibModalInstance.dismiss('cancel');
  };
//End_Function time->input


    /*------------Função: $ctrl.conferirHorario------------
    
    Responsável por conferir/validar a hora que foi adicionada no input

    ---------------------------------------------------*/    
    $ctrl.conferirHorario = function(){
        if( typeof $ctrl.atracao.opening_hours == 'undefined' || typeof $ctrl.atracao.opening_hours.periods == 'undefined'){
            return "valido";
        }
        var dias = $ctrl.atracao.opening_hours.periods;
        if(dias.length == 1 && dias[0].open.day == 0 && dias[0].open.time == "0000"){
            return "valido";
        }

        var diaParts = $ctrl.dia.split("/");
        diaParts[0] = parseInt(diaParts[0]);
        diaParts[1] = parseInt(diaParts[1]);
        diaParts[2] = parseInt(diaParts[2]);

        var dataEscolhida = new Date(diaParts[2], diaParts[1] - 1, diaParts[0]);

        for (var i = dias.length - 1; i >= 0; i--) {
            if(dias[i].open.day == dataEscolhida.getDay()){
                var hora = $ctrl.mytime.split(":");
                dataEscolhida.setHours(hora[0]);
                dataEscolhida.setMinutes(hora[1]);

                var dataOpen = new Date(dataEscolhida);
                dataOpen.setHours(parseInt(dias[i].open.time.slice(0,2)));
                dataOpen.setMinutes(parseInt(dias[i].open.time.slice(2,4)));

                if(parseInt(dias[i].open.time)>parseInt(dias[i].close.time)){
                    var dataClose = new Date(dataEscolhida);
                    dataClose.setHours(parseInt(dias[i].close.time.slice(0,2)));
                    dataClose.setMinutes(parseInt(dias[i].close.time.slice(2,4)));
                    dataClose.setDate(dataEscolhida.getDate()+1);
                }else{
                    var dataClose = new Date(dataEscolhida);
                    dataClose.setHours(parseInt(dias[i].close.time.slice(0,2)));
                    dataClose.setMinutes(parseInt(dias[i].close.time.slice(2,4)));
                }
                if(dataEscolhida>=dataOpen && dataEscolhida<=dataClose){
                    return "valido";
                }
            }
        }

        return "invalido";
    }


    /*------------Função: $ctrl.conferirData------------
    
    Responsável por conferir/validar a data que foi adicionada no input

    ---------------------------------------------------*/
    $ctrl.conferirData = function(){
        //divisão da string do input para separar dia, mês e ano
        var diaParts = $ctrl.dia.split("/");
        diaParts[0] = parseInt(diaParts[0]);
        diaParts[1] = parseInt(diaParts[1]);
        diaParts[2] = parseInt(diaParts[2]);
        
        if(diaParts[0]>0 && diaParts[0]<9 && diaParts[1]>0 && diaParts[1]<9){
            var error = "0"+diaParts[0]+"/0"+diaParts[1]+"/"+diaParts[2];
        }else if(diaParts[0]>0 && diaParts[0]<9){
            var error = "0"+diaParts[0]+"/"+diaParts[1]+"/"+diaParts[2];
        }else if(diaParts[1]>0 && diaParts[1]<9){
            var error = diaParts[0]+"/0"+diaParts[1]+"/"+diaParts[2];
        }else{
            var error = diaParts[0]+"/"+diaParts[1]+"/"+diaParts[2];
        }
        if(error != $ctrl.dia){
            return "invalido";
        }

        var initialParts = $rootScope.infoUser.data_inicio.split("/");
        initialParts[0] = parseInt(initialParts[0]);
        initialParts[1] = parseInt(initialParts[1]);
        initialParts[2] = parseInt(initialParts[2]);

        var finalParts = $rootScope.infoUser.data_fim.split("/");
        finalParts[0] = parseInt(finalParts[0]);
        finalParts[1] = parseInt(finalParts[1]);
        finalParts[2] = parseInt(finalParts[2]);

        //se o dia, mês e ano são válidos   
        if(diaParts[1]==1 || diaParts[1]==3 || diaParts[1]==5 || diaParts[1]==7 || diaParts[1]==8 || diaParts[1]==10 || diaParts[1]==12){
            if(diaParts[0]<1 || diaParts[0]>31){
                return "invalido";
            }
        }else if(diaParts[1]==4 || diaParts[1]==6 || diaParts[1]==9 || diaParts[1]==11){
            if(diaParts[0]<1 || diaParts[0]>30){
                return "invalido";
            }
        }else if(diaParts[1]==2){
            if((diaParts[2]%4 == 0) && (diaParts[2]%100 != 0 || diaParts[2]%400 == 0)){
                if(diaParts[0]<1 || diaParts[0]>29){
                    return "invalido";
                }
            }else{
                if(diaParts[0]<1 || diaParts[0]>28){
                    return "invalido";
                }
            }
        }else if(diaParts[1]>12){
            return "invalido";
        }

        //se a data está entre o período da estadia
        var dataEscolhida = new Date(diaParts[2], diaParts[1] - 1, diaParts[0]);
        var dataInicial = new Date(initialParts[2], initialParts[1] - 1, initialParts[0]);
        var dataFinal = new Date(finalParts[2], finalParts[1] - 1, finalParts[0]);

        if(dataEscolhida<dataInicial){
            return "invalido";
        }
        if(dataFinal<dataEscolhida){
            return "invalido";
        }

        return "valido";
    }
});