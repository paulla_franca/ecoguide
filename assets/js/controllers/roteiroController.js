app.controller("roteiroController", function($scope, $http, $location, $sce, $rootScope){

    //variável global com todas as informações do roteiro
    $rootScope.roteiro = [];

    //função responsável por gerar o roteiro e adicioná-lo no array $rootScope.roteiro
    $scope.gerarRoteiro = function(){
        //seleciona a quantidade de dias que passará na viagem
        var initialParts = $rootScope.infoUser.data_inicio.split("/");
        var finalParts = $rootScope.infoUser.data_fim.split("/");

        var dataInicial = new Date(parseInt(initialParts[2]), parseInt(initialParts[1]) - 1, parseInt(initialParts[0]));
        var dataFinal = new Date(parseInt(finalParts[2]), parseInt(finalParts[1]) - 1, parseInt(finalParts[0]));

        var diferencaTempo = Math.abs(dataFinal.getTime() - dataInicial.getTime());
        var qtdDias = Math.ceil(diferencaTempo / (1000 * 3600 * 24));

        //vetor das atividades disponiveis escolhidas pelo usuário
        var atividades = [];

        //adiciona o hotel às atividades
        for(var i = 0; i <= qtdDias; i++){
            var data = new Date(dataInicial);
            data.setDate(dataInicial.getDate() + i);
            var start = new Date(data);
            start.setHours(0);
            start.setMinutes(0);
            var end = new Date(data);
            end.setHours(7);
            end.setMinutes(0);
            var atividade = {
                "OwnerID": 1,
                formatted_address: $rootScope.infoUser.hotel.formatted_address,
                title: $rootScope.infoUser.hotel.name,
                image: $rootScope.infoUser.hotel.photos[0],
                start: start,
                end: end
            }
            atividades.push(atividade);
            var start2 = new Date(data);
            start2.setHours(21);
            start2.setMinutes(0);
            var end2 = new Date(data);
            end2.setHours(24);
            end2.setMinutes(0);
            var atividade = {
                "OwnerID": 1,
                formatted_address: $rootScope.infoUser.hotel.formatted_address,
                title: $rootScope.infoUser.hotel.name,
                image: $rootScope.infoUser.hotel.photos[0],
                start: start2,
                end: end2
            }
            atividades.push(atividade);
        }
        //adiciona os restaurantes às atividades
        for(var i = 0; i < $rootScope.infoUser.restaurantes.length; i++){
            var dataAtvParts = $rootScope.infoUser.restaurantes[i].dia_escolhido.split("/");
            var dataAtv = new Date(parseInt(dataAtvParts[2]), parseInt(dataAtvParts[1]) - 1, parseInt(dataAtvParts[0]));
            var horario = $rootScope.infoUser.restaurantes[i].horario_escolhido.split(":");
            var start = new Date(dataAtv);
            start.setHours(horario[0]);
            start.setMinutes(horario[1]);
            var end = new Date(start);
            end.setHours(start.getHours()+3);
            var atividade = {
                "OwnerID": 2,
                formatted_address: $rootScope.infoUser.hotel.formatted_address,
                title: $rootScope.infoUser.restaurantes[i].nome,
                image: $rootScope.infoUser.restaurantes[i].photos[0],
                start: start,
                end: end
            }
            atividades.push(atividade);
        }
        //adiciona as atrações às atividades
        for(var i = 0; i < $rootScope.infoUser.atracoes.length; i++){
            var dataAtvParts = $rootScope.infoUser.atracoes[i].dia_escolhido.split("/");
            var dataAtv = new Date(parseInt(dataAtvParts[2]), parseInt(dataAtvParts[1]) - 1, parseInt(dataAtvParts[0]));
            var horario = $rootScope.infoUser.atracoes[i].horario_escolhido.split(":");
            var start = new Date(dataAtv);
            start.setHours(horario[0]);
            start.setMinutes(horario[1]);
            var end = new Date(start);
            end.setHours(start.getHours()+3);
            var atividade = {
                "OwnerID": 3,
                formatted_address: $rootScope.infoUser.hotel.formatted_address,
                title: $rootScope.infoUser.atracoes[i].nome,
                image: $rootScope.infoUser.atracoes[i].photos[0],
                start: start,
                end: end
            }
            atividades.push(atividade);
        }
        $rootScope.roteiro = atividades;
        console.log($rootScope.roteiro)
    }

    $scope.gerarRoteiro();
});