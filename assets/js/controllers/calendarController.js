 app.controller("calendarController", function($scope, $rootScope){
        var initialParts = $rootScope.infoUser.data_inicio.split("/");

        var dataInicial = new Date(parseInt(initialParts[2]), parseInt(initialParts[1]) - 1, parseInt(initialParts[0]));
        dataInicial.setHours(0);
        dataInicial.setMinutes(0);

        $scope.schedulerOptions = {
            toolbar: [ "pdf" ],
            pdf: {
                fileName: "Kendo UI Scheduler Export.pdf",
                proxyURL: "https://demos.telerik.com/kendo-ui/service/export"
            },
            date: dataInicial,
            startTime: dataInicial,
            endTime: dataInicial,
            eventTemplate: $("#event-template").html(),
            height: 600,
            views: [
                "day",
                { type: "week", selected: true },
                "month",
                "agenda",
                { type: "timeline", eventHeight: 50}
            ],
            dataSource: $rootScope.roteiro,
            resources: [
                {
                    field: "ownerId",
                    title: "Owner",
                    dataSource: [
                        { text: "Hotel", value: 1, color: "#f8a398" },
                        { text: "Restaurante", value: 2, color: "#51a0ed" },
                        { text: "Atrações", value: 3, color: "#56ca85" }
                    ]
                }
            ]
        };
      })