
app.controller("estadiaController", function($scope, $http, $location, $sce, $rootScope){

    autocomplete = new google.maps.places.Autocomplete(document.getElementById("destino"));

    //Chave de acesso à API do Google Places
    //$rootScope.key = "AIzaSyC7kbtES2H7cMUswCn27YfaAGWz2QIaXjo";
    
    //Variável global para guardar todas as informações do usuário
    $rootScope.infoUser = {
        "destino": "",
        "destino_id": "",
        "data_inicio": "",
        "data_fim": "",
        "hotel_nome": "",
        "hotel_id": "",
        "restaurantes": [],
        "atracoes": [],
        "distancia": ""
    };

    /*------------Função: $scope.inserir------------
    
    Insere as informações preenchidas da página estadia na variável global "infoUser"

    ---------------------------------------------------*/
    $scope.inserir = function(){
        var place = autocomplete.getPlace();
        if($scope.destino == "" || $scope.destino == null || $scope.initialDay == "" || $scope.initialDay == null || $scope.finalDay == "" || $scope.finalDay == null){
            alert("Algumas informações estão vazias");
        }
        if($scope.conferirData() == "invalido"){
            alert("As datas escolhidas não são válidas");
        }
        if($scope.destino != "" && $scope.initialDay != "" && $scope.finalDay != "" && $scope.conferirData() == "valido"){
            $rootScope.infoUser.destino = $scope.destino;
            $rootScope.infoUser.destino_id = place.place_id;
            $rootScope.infoUser.data_inicio = $scope.initialDay;
            $rootScope.infoUser.data_fim = $scope.finalDay;
            $rootScope.infoUser.distancia = $scope.distancia;
            $location.path('/hoteis');
        }
    }

    /*------------Função: $scope.conferirData------------
    
    Responsável por conferir/validar as datas que foram adicionadas nos inputs da estadia

    ---------------------------------------------------*/
    $scope.conferirData = function(){
        //divisão da string do input para separar dia, mês e ano
        var initialParts = $scope.initialDay.split("/");
        initialParts[0] = parseInt(initialParts[0]);
        initialParts[1] = parseInt(initialParts[1]);
        initialParts[2] = parseInt(initialParts[2]);

        var finalParts = $scope.finalDay.split("/");
        finalParts[0] = parseInt(finalParts[0]);
        finalParts[1] = parseInt(finalParts[1]);
        finalParts[2] = parseInt(finalParts[2]);

        //conferir se a data é formada por apenas números
        if(initialParts[0]>0 && initialParts[0]<9 && initialParts[1]>0 && initialParts[1]<9){
            var error = "0"+initialParts[0]+"/0"+initialParts[1]+"/"+initialParts[2];
        }else if(initialParts[0]>0 && initialParts[0]<9){
            var error = "0"+initialParts[0]+"/"+initialParts[1]+"/"+initialParts[2];
        }else if(initialParts[1]>0 && initialParts[1]<9){
            var error = initialParts[0]+"/0"+initialParts[1]+"/"+initialParts[2];
        }else{
            var error = initialParts[0]+"/"+initialParts[1]+"/"+initialParts[2];
        }
        if(finalParts[0]>0 && finalParts[0]<9 && finalParts[1]>0 && finalParts[1]<9){
            var error2 = "0"+finalParts[0]+"/0"+finalParts[1]+"/"+finalParts[2];
        }else if(finalParts[0]>0 && finalParts[0]<9){
            var error2 = "0"+finalParts[0]+"/"+finalParts[1]+"/"+finalParts[2];
        }else if(finalParts[1]>0 && finalParts[1]<9){
            var error2 = finalParts[0]+"/0"+finalParts[1]+"/"+finalParts[2];
        }else{
            var error2 = finalParts[0]+"/"+finalParts[1]+"/"+finalParts[2];
        }
        if(error != $scope.initialDay || error2 != $scope.finalDay){
            return "invalido";
        }

        //se o dia, mês e ano são válidos   
        if(initialParts[1]==1 || initialParts[1]==3 || initialParts[1]==5 || initialParts[1]==7 || initialParts[1]==8 || initialParts[1]==10 || initialParts[1]==12){
            if(initialParts[0]<1 || initialParts[0]>31){
                return "invalido";
            }
        }else if(initialParts[1]==4 || initialParts[1]==6 || initialParts[1]==9 || initialParts[1]==11){
            if(initialParts[0]<1 || initialParts[0]>30){
                return "invalido";
            }
        }else if(initialParts[1]==2){
            if((initialParts[2]%4 == 0) && (initialParts[2]%100 != 0 || initialParts[2]%400 == 0)){
                if(initialParts[0]<1 || initialParts[0]>29){
                    return "invalido";
                }
            }else{
                if(initialParts[0]<1 || initialParts[0]>28){
                    return "invalido";
                }
            }
        }else if(initialParts[1]>12){
            return "invalido";
        }

        if(finalParts[1]==1 || finalParts[1]==3 || finalParts[1]==5 || finalParts[1]==7 || finalParts[1]==8 || finalParts[1]==10 || finalParts[1]==12){
            if(finalParts[0]<1 || finalParts[0]>31){
                return "invalido";
            }
        }else if(finalParts[1]==4 || finalParts[1]==6 || finalParts[1]==9 || finalParts[1]==11){
            if(finalParts[0]<1 || finalParts[0]>30){
                return "invalido";
            }
        }else if(finalParts[1]==2){
            if((finalParts[2]%4 == 0) && (finalParts[2]%100 != 0 || finalParts[2]%400 == 0)){
                if(finalParts[0]<1 || finalParts[0]>29){
                    return "invalido";
                }
            }else{
                if(finalParts[0]<1 || finalParts[0]>28){
                    return "invalido";
                }
            }
        }else if(finalParts[1]>12){
            return "invalido";
        }

        //se as datas são hoje ou dias depois
        var dataAtual = new Date();
        var dataInicial = new Date(initialParts[2], initialParts[1] - 1, initialParts[0]);
        var dataFinal = new Date(finalParts[2], finalParts[1] - 1, finalParts[0]);

        if(dataInicial<dataAtual){
            return "invalido";
        }
        if(dataFinal<dataAtual){
            return "invalido";
        }

        //se a data inicial é menor do que a data final
        if(dataInicial>dataFinal){
            return "invalido";
        }

        return "valido";
    }
    

});