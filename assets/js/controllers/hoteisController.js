app.controller("hoteisController", function($scope, $http, $sce, $rootScope, $uibModal, $log, $document, $timeout){
    $scope.apear = function(){
        $scope.hoteisDetalhe = [];
        
        var service = new google.maps.places.PlacesService(document.createElement('div'));

        service.getDetails({
          placeId: $rootScope.infoUser.destino_id
        }, function(place, status) {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            var local = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
            service.nearbySearch({
                location: local,
                radius: $rootScope.infoUser.distancia,
                type: ['lodging']
            },  function(results, status){
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        service.getDetails({
                            placeId:results[i].place_id
                        }, function(place, status){
                            if (status === google.maps.places.PlacesServiceStatus.OK) {
                                if(typeof place.photos != 'undefined'){
                                    for (var i = place.photos.length - 1; i >= 0; i--) {
                                        place.photos[i] = place.photos[i].getUrl({ 'maxWidth': 205, 'maxHeight': 150 });
                                    }
                                }else{
                                    place.photos = [];
                                    place.photos[0] = "assets/images/image.png";
                                }
                                $scope.hoteisDetalhe.push(place);
                            }
                        });                        
                    }
                }
            });
          }
        });

        $timeout(function () {
            console.log($scope.hoteisDetalhe);
        }, 2000);
    }
    $scope.apear();
    //função para abrir modal
    $scope.openModal = function(hotel, size, parentSelector){
        var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'myModalContent1.html',
            controller: 'ModalInstanceCtrl1',
            controllerAs: '$ctrl',
            size: size,
            appendTo: parentElem,
            resolve: {
                hotel: function () {
                    return hotel;
                }
            }
        });
    }

});

//ModalController -> controller usado dentro do modal (tudo o que tiver dentro da div do modal, as funções para isso (o que está dentro da divModal) ficam dentro desse controller)
app.controller('ModalInstanceCtrl1', function ($rootScope, $scope, $uibModalInstance, $log, hotel) {
  var $ctrl= this;
  $ctrl.hotel = null;
  $ctrl.hotel = hotel;

  $ctrl.selecionarHotel = function(){
    $rootScope.infoUser.hotel = hotel;
    $uibModalInstance.close("");
  };
  $ctrl.cancel = function(){
    $ctrl.hotel = null;
    $uibModalInstance.dismiss('cancel');
  };

});