var app = angular.module("ecoguide", ['ui.router', 'ngMask', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'kendo.directives']);
app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state({
                name: "estadia2",
                url: "#sistema",
                templateUrl: 'view/estadia.php/',
                controller: 'estadiaController'
            })
            .state({
                name: "estadia",
                url: "/",
                templateUrl: 'view/estadia.php/',
                controller: 'estadiaController'
            })
            .state({
                name: "hoteis",
                url: "/hoteis",
                templateUrl: 'view/hoteis.html',
                controller: 'hoteisController'
            })
            .state({
                name: "restaurantes",
                url: "/restaurantes",
                templateUrl: 'view/restaurantes.html',
                controller: 'restaurantesController'
            })
            .state({
                name: "atracoes",
                url: "/atracoes",
                templateUrl: 'view/atracoes.html',
                controller: 'atracoesController'
            })
            .state({
                name: "roteiro",
                url: "/roteiro",
                templateUrl: 'view/roteiro.php',
                controller: 'roteiroController'
            });
        $urlRouterProvider.otherwise('/');
    }]);